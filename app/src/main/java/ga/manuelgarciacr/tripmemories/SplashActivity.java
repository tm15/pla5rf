package ga.manuelgarciacr.tripmemories;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.firebase.ui.auth.AuthUI;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Collections;

import static ga.manuelgarciacr.tripmemories.MainActivity.LOGOUT_EXTRA;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 1900;
    private static final int MY_SIGN_IN = 1;
    ImageView mImvSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseFirestore.setLoggingEnabled(true);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = getIntent();
        if(intent.getStringExtra(LOGOUT_EXTRA) != null || shouldStartSignIn())
            startSignIn();
        else
            startMain();
    }

    private boolean shouldStartSignIn() {
        return (FirebaseAuth.getInstance().getCurrentUser() == null);
    }

    private void startSignIn() {
        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
                .setAvailableProviders(Collections.singletonList(
                        new AuthUI.IdpConfig.EmailBuilder().build()))
                .setIsSmartLockEnabled(!BuildConfig.DEBUG)
                .setTheme(R.style.FirebaseUI)
                .setLogo(R.drawable.ic_launcher_foreground)
                .build();
        startActivityForResult(intent, MY_SIGN_IN);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == MY_SIGN_IN) {
            if (resultCode != RESULT_OK && shouldStartSignIn())
                startSignIn();
            else
                startMain();
        }
    }

    private void startMain(){
        setContentView(R.layout.activity_splash);
        mImvSplash=findViewById(R.id.imvSplash);
        Animation an= AnimationUtils.loadAnimation(this,R.anim.scale);
        mImvSplash.startAnimation(an);
        new Handler().postDelayed(() -> {
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        }, SPLASH_TIME_OUT);
    }
}
