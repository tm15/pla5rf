package ga.manuelgarciacr.tripmemories.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.view.View;

import java.io.File;

import static ga.manuelgarciacr.tripmemories.util.utils.log;
import static java.security.AccessController.getContext;

public class PictureUtils {
    public static Bitmap getScaledBitmapFromFile(File file, Activity activity, View view){
        if (file != null && file.exists()) {
            return getScaledBitmap(file.getPath(), activity, view);
        }else
            return null;

    }

    public static Bitmap getScaledBitmapFromFile(File file, Activity activity){
        return getScaledBitmapFromFile(file, activity, null);
    }

    public static Bitmap getScaledBitmap(String path, Activity activity, View view) {
        int w, nw, h;
        Point size = new Point();

        activity.getWindowManager().getDefaultDisplay()
                .getSize(size);
        w = size.x;
        h = size.y;

        if(view != null)
            if((nw = view.getLayoutParams().width) > 0) {
                w = nw;
                h = view.getLayoutParams().width;
            }else if((nw = view.getWidth()) > 0) {
                w = nw;
                h = view.getHeight();
            }

        return getScaledBitmap(path, w, h);
    }

    /*
    public static Bitmap getScaledBitmap(String path, Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay()
                .getSize(size);
        return getScaledBitmap(path, size.x, size.y);
    }
    */

    public static Bitmap getScaledBitmap(String path, int destWidth, int destHeight) {
        log("GSB", path, Integer.toString(destWidth), Integer.toString(destHeight));
        // Read in the dimensions of the image on disk
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        float srcWidth = options.outWidth;
        float srcHeight = options.outHeight;
        // Figure out how much to scale down by
        int inSampleSize = 1;
        if (srcHeight > destHeight || srcWidth > destWidth) {
            float heightScale = srcHeight / destHeight;
            float widthScale = srcWidth / destWidth;
            inSampleSize = Math.round(heightScale > widthScale ? heightScale :
                    widthScale);
        }
        options = new BitmapFactory.Options();
        options.inSampleSize = inSampleSize;
        return BitmapFactory.decodeFile(path, options);
    }
}
// Read in and create final bitmap
