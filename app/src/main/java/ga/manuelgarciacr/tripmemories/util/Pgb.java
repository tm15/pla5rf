package ga.manuelgarciacr.tripmemories.util;

import android.widget.ProgressBar;
import android.widget.TextView;

public class Pgb {
    private ProgressBar bar;
    private TextView txv;
    private int count;

    public Pgb(ProgressBar bar, TextView txv) {
        setBar(bar);
        setTxv(txv);
        setCount(0);
    }

    public void add(String prompt){
        setCount(getCount() + 1);
        getTxv().setText(prompt);
        getBar().setVisibility(ProgressBar.VISIBLE);
    }

    public void remove(){
        if (setCount(getCount() - 1) <= 0) {
            setCount(0);
            getTxv().setText("");
            getBar().setVisibility(ProgressBar.INVISIBLE);
        }
    }

    private ProgressBar getBar() {
        return bar;
    }

    private void setBar(ProgressBar bar) {
        this.bar = bar;
    }

    private TextView getTxv() {
        return txv;
    }

    private void setTxv(TextView txv) {
        this.txv = txv;
    }

    private int getCount() {
        return count;
    }

    private int setCount(int count) {
        this.count = count;
        return getCount();
    }

}
