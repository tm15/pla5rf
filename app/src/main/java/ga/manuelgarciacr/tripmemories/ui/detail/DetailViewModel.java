package ga.manuelgarciacr.tripmemories.ui.detail;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.UUID;

import ga.manuelgarciacr.tripmemories.model.Trip;
import ga.manuelgarciacr.tripmemories.model.TripRepository;

@SuppressWarnings("unused")
public class DetailViewModel extends AndroidViewModel {
    private TripRepository tripRepository;
    private MutableLiveData<UUID> tripIdLiveData = new MutableLiveData<>();
    LiveData<Trip> tripLiveData =
            Transformations.switchMap(tripIdLiveData, mUUID ->
                    tripRepository.getTripById(mUUID));
    private LiveData<Trip> mUpdateResult;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        tripRepository = TripRepository.getInstance(application);
    }
/*
    void loadUUID(UUID mTripId) {
        tripIdLiveData.setValue(mTripId);
    }

    void updateTrip(Trip trip) {
        tripRepository.updateTrip(trip);
    }

    public void insertTrip(Trip trip) {
        tripRepository.insertTrip(trip);
    }

    void deleteTrip(Trip trip) {
        tripRepository.deleteTrip(trip);
    }

    public void updateTrip(Trip trip) {
        mUpdateResult = tripRepository.insertTrip(trip);
    }

    LiveData<Trip> getUpdateResult() {
        return mUpdateResult;
    }

    void deleteTrip(Trip trip) {
        tripRepository.deleteTrip(trip.getUuid());
    }

 */
}
